import numpy as np
from sklearn.neighbors import NearestNeighbors
def kNN(locations,k):
    nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(locations)
    dist, index = nbrs.kneighbors(locations)
    out = {}
    for i in index:
        curr = str(locations[i[0]])
        nbs = []
        nb = list(locations[list(i[1:])])
        for j in nb:
            x = list(j)
            nbs.append(x)
        out[curr] = nbs
    return out